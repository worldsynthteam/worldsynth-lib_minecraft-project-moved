package com.booleanbyte.worldsynthlib.minecraft;

import com.booleanbyte.worldsynth.module.AbstractModuleRegister;
import com.booleanbyte.worldsynth.module.ClassNotModuleExeption;
import com.booleanbyte.worldsynthlib.minecraft.module.anvil.ModuleMinecraftWorldExport;
import com.booleanbyte.worldsynthlib.minecraft.module.anvil.ModuleMinecraftWorldImport;

public class MinecraftModuleRegister extends AbstractModuleRegister {
	
	public MinecraftModuleRegister() {
		super();
		
		try {
			registerModule(ModuleMinecraftWorldExport.class, "\\Minecraft");
			registerModule(ModuleMinecraftWorldImport.class, "\\Minecraft");
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
}
