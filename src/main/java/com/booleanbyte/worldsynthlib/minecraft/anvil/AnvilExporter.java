package com.booleanbyte.worldsynthlib.minecraft.anvil;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class AnvilExporter {
	
	private static final String NETHER_FOLDER = "DIM-1";
    private static final String ENDER_FOLDER = "DIM1";
    
    private File overworldRegionFolder;
    private File netherRegionFolder;
    private File endRegionFolder;
	
	private File saveDirectory;
	private File levelDirectory;
	
	private String levelId;
	private String generatorName;
	
	public AnvilExporter(File saveDirectory, String levelName, String generatorName) {
		if(!saveDirectory.exists()) {
			return;
		}
		
		this.levelId = levelName;
		this.generatorName = generatorName;
		levelDirectory = new File(saveDirectory, levelId);
		if(!levelDirectory.exists()) {
			levelDirectory.mkdir();
		}

		overworldRegionFolder = new File(levelDirectory, "region");
		netherRegionFolder = new File(levelDirectory, NETHER_FOLDER + "/region");
		endRegionFolder = new File(levelDirectory, ENDER_FOLDER + "/region");

		overworldRegionFolder.mkdir();
		netherRegionFolder.mkdirs();
		endRegionFolder.mkdirs();

		createLevelFile();
		
		// Write session.lock file
        File sessionLockFile = new File(levelDirectory, "session.lock");
    	DataOutputStream sessionOut;
    	try {
    		sessionOut = new DataOutputStream(new FileOutputStream(sessionLockFile));
    		sessionOut.writeLong(System.currentTimeMillis());
    		sessionOut.close();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	private void createLevelFile() {
		Level level = new Level(levelId, generatorName, 0, 0, 100, 0);
		CompoundTag levelCompoundTag = level.toNbt();
		
		saveDataTag(levelDirectory, levelCompoundTag);
	}
	
	private void saveDataTag(File levelDir, CompoundTag dataTag) {
        File dataFile = new File(levelDir, "level.dat");
        try {
            CompoundTag root = new CompoundTag("");
            root.put(dataTag);
            
            NBTIO.writeFile(root, dataFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public void writeChunkToFile(Chunk chunk, Dimension dim) {
		double chunkX = chunk.getX();
		double chunkZ = chunk.getZ();
		
		int regionX = (int) Math.floor(chunkX / 32.0);
		int regionZ = (int) Math.floor(chunkZ / 32.0);
		
		int regionChunkX = chunk.getX() - regionX * 32;
		int regionChunkZ = chunk.getZ() - regionZ * 32;
		
		RegionFile region = null;
		if(dim == Dimension.OVERWORLD) {
			region = new RegionFile(overworldRegionFolder, regionX, regionZ);
		}
		else if(dim == Dimension.NETHER) {
			region = new RegionFile(netherRegionFolder, regionX, regionZ);
		}
		else if(dim == Dimension.END) {
			region = new RegionFile(endRegionFolder, regionX, regionZ);
		}
		
		CompoundTag chunkData = chunk.toNbt();
		OutputStream chunkDataOutputStream = region.getChunkDataOutputStream(regionChunkX, regionChunkZ);
		try {
			NBTIO.writeTag(chunkDataOutputStream, chunkData);
			chunkDataOutputStream.close();
			region.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
