package com.booleanbyte.worldsynthlib.minecraft.anvil;

import com.booleanbyte.worldsynth.biome.BiomeRegistry;
import com.booleanbyte.worldsynth.material.MaterialRegistry;
import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntArrayTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;

/**
 * 
 * Based on information from
 * https://minecraft.gamepedia.com/Chunk_format
 */

public class Chunk {
	
	public static final String TAG_CHUNK = "Chunk";
	public static final String TAG_DATAVERSION = "DataVersion";
	public static final String TAG_LEVEL = "Level";
	public static final String TAG_XPOS = "xPos";
	public static final String TAG_ZPOS = "zPos";
	public static final String TAG_LASTUPDATE = "LastUpdate";
	public static final String TAG_INHABITEDTIME = "InhabitedTime";
	public static final String TAG_LIGHTPOPULATED = "LightPopulated";
	public static final String TAG_TERRAINPOPULATED = "TerrainPopulated";
	public static final String TAG_BIOMES = "Biomes";
	public static final String TAG_HEIGHTMAP = "HeightMap";
	public static final String TAG_SECTIONS = "Sections";
	public static final String TAG_ENTITIES = "Entities";
	public static final String TAG_TILEENTITIES = "TileEntities";
	
	private int dataVersion;
	
	private int xPos;
	private int zPos;
	
	private long lastUpdate = 0;
	
	private byte lightPopulated;
	private byte terrainPopulated = 1;
	
	private long inhabitedTime = 0;
	
	/**
	 * Biomes defined in minecraft ids
	 */
	private int[][] biomes = new int[16][16]; //If biomes are not defined, set to -1 to make minecraft generate it
	private int[][] heightmap = new int[16][16];
	
	private byte sectionsCount;
	private ChunkSection[] sections = new ChunkSection[16];
	
	private Entety[] entities;
	private Entety[] tileEntities;
	
	private TileTick[] tileTicks;
	
	//Expects biomes and materials in internal worldsynth ids
	public Chunk(int xPos, int zPos, int[][] biomes, int[][][] materials, boolean populated) {
		this.xPos = xPos;
		this.zPos = zPos;
		
		this.biomes = biomes;
		//Convert biomes from internal worldsynth biome ids to minecraft ids
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				this.biomes[x][z] = BiomeRegistry.getBiomeByInternalId(this.biomes[x][z]).getId();
			}
		}
		
		if(!populated) {
			this.terrainPopulated = 0;
		}
		
		//Generate heightmap
		int maxHeight = 0;
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				for(int y = 255; y >= 0; y--) {
					if(materials[x][y][z] != 0) {
						heightmap[x][z] = y;
						if(y > maxHeight) {
							maxHeight = y;
						}
						break;
					}
				}
			}
		}
		
		//Generate sections
		sectionsCount = (byte) Math.ceil((double)(maxHeight+1)/16.0);
		for(byte i = 0; i < sectionsCount; i++) {
			int[][][] blockId = new int[16][16][16];
			byte[][][] blockData = new byte[16][16][16];
			
			for(int x = 0; x < 16; x++) {
				for(int z = 0; z < 16; z++) {
					for(int y = 0; y < 16; y++) {
						blockId[x][y][z] = MaterialRegistry.getMaterialByInternalId(materials[x][i*16+y][z]).getId();
						blockData[x][y][z] = MaterialRegistry.getMaterialByInternalId(materials[x][i*16+y][z]).getMeta();
					}
				}
			}
			
			sections[i] = new ChunkSection(i, blockId, blockData);
		}
	}
	
	public Chunk(CompoundTag chunkTag) {
		dataVersion = (int) chunkTag.get(TAG_DATAVERSION).getValue();
		
		CompoundTag levelTag = (CompoundTag) chunkTag.get(TAG_LEVEL);
		xPos = (int) levelTag.get(TAG_XPOS).getValue();
		zPos = (int) levelTag.get(TAG_ZPOS).getValue();
		lastUpdate = (long) levelTag.get(TAG_LASTUPDATE).getValue();
		inhabitedTime = (long) levelTag.get(TAG_INHABITEDTIME).getValue();
		lightPopulated = (byte) levelTag.get(TAG_LIGHTPOPULATED).getValue();
		terrainPopulated = (byte) levelTag.get(TAG_TERRAINPOPULATED).getValue();
		
		byte[] serializedBiomeArray = ((ByteArrayTag) levelTag.get(TAG_BIOMES)).getValue();
		int[] serializedHeightMap = ((IntArrayTag) levelTag.get(TAG_HEIGHTMAP)).getValue();
		ListTag sectionsTagList = (ListTag) levelTag.get(TAG_SECTIONS);
		ListTag entitiesTagList = (ListTag) levelTag.get(TAG_ENTITIES);
		ListTag tileEntitiesTagList = (ListTag) levelTag.get(TAG_TILEENTITIES);
		
		//Convert biomemap and heightmap
		biomes = new int[16][16];
		heightmap = new int[16][16];
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				biomes[x][z] = serializedBiomeArray[x + z*16] & 0xFF;
				heightmap[x][z] = serializedHeightMap[x + z*16];
			}
		}
		
		//Convert sections
		sectionsCount = (byte) sectionsTagList.size();
		for(int i = 0; i < sectionsCount; i++) {
			sections[i] = new ChunkSection((CompoundTag) sectionsTagList.get(i));
		}
		
		//TODO Entities and tileentities to be implemented when the need arrives
	}
	
	public int[][] getHeightmap() {
		return heightmap;
	}
	
	/**
	 * Gets biomemap data where biomes are represented in internal worldsynth biome ids
	 */
	public int[][] getBiomeMap() {
		int[][] returnBiomeMap = new int[biomes.length][biomes[0].length];
		
		for(int x = 0; x < biomes.length; x++) {
			for(int z = 0; z < biomes[0].length; z++) {
				returnBiomeMap[x][z] = BiomeRegistry.getBiome(biomes[x][z]).getInternalId();
			}
		}
		
		return returnBiomeMap;
	}
	
	private int[][][] getBlockId() {
		int[][][] blockId = new int[16][256][16];
		
		for(int i = 0; i < sectionsCount; i++) {
			for(int x = 0; x < 16; x++) {
				for(int y = 0; y < 16; y++) {
					for(int z = 0; z < 16; z++) {
						blockId[x][i*16 + y][z] = sections[i].blockId[x][y][z];
					}
				}
			}
		}
		
		return blockId;
	}
	
	private byte[][][] getBlockData() {
		byte[][][] blockData = new byte[16][256][16];
		
		for(int i = 0; i < sectionsCount; i++) {
			for(int x = 0; x < 16; x++) {
				for(int y = 0; y < 16; y++) {
					for(int z = 0; z < 16; z++) {
						blockData[x][i*16 + y][z] = sections[i].blockData[x][y][z];
					}
				}
			}
		}
		
		return blockData;
	}
	
	/**
	 * Gets blockspace where blocks are represented in internal worldsynth material ids
	 */
	public int[][][] getBlockspaceMaterial() {
		int[][][] blockspaceMaterial = new int[16][256][16];
		
		int[][][] blockId = getBlockId();
		byte[][][] blockData = getBlockData();
		
		for(int x = 0; x < 16; x++) {
			for(int y = 0; y < 256; y++) {
				for(int z = 0; z < 16; z++) {
					blockspaceMaterial[x][y][z] = MaterialRegistry.getMaterial(blockId[x][y][z], blockData[x][y][z]).getInternalId();
				}
			}
		}
		
		return blockspaceMaterial;
	}
	
	public CompoundTag toNbt() {
		//Create level compound tag
		CompoundTag levelCompoundTag = new CompoundTag(TAG_LEVEL);
		levelCompoundTag.put(new IntTag(TAG_XPOS, xPos));
		levelCompoundTag.put(new IntTag(TAG_ZPOS, zPos));
		
		levelCompoundTag.put(new LongTag(TAG_INHABITEDTIME, inhabitedTime));
		levelCompoundTag.put(new LongTag(TAG_LASTUPDATE, lastUpdate));
		levelCompoundTag.put(new ByteTag(TAG_LIGHTPOPULATED, lightPopulated));
		levelCompoundTag.put(new ByteTag(TAG_TERRAINPOPULATED, terrainPopulated));
		
		byte[] biomeByteArray = new byte[256];
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				biomeByteArray[x+16*z] = (byte) biomes[x][z];
			}
		}
		levelCompoundTag.put(new ByteArrayTag(TAG_BIOMES, biomeByteArray));
		
		int[] HeightIntegerArray = new int[256];
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				HeightIntegerArray[x+16*z] = heightmap[x][z];
			}
		}
		levelCompoundTag.put(new IntArrayTag(TAG_HEIGHTMAP, HeightIntegerArray));
		
		ListTag sectionsListTag = new ListTag(TAG_SECTIONS);
		for(int i = 0; i < sectionsCount; i++) {
			if(sections[i] != null) {
				sectionsListTag.add(sections[i].toNbt());
			}
		}
		levelCompoundTag.put(sectionsListTag);
		
		//TODO Entities and tileentities to be implemented when the need arrives
		ListTag entitiesListTag = new ListTag(TAG_ENTITIES);
		levelCompoundTag.put(entitiesListTag);
		
		ListTag tileEntitiesListTag = new ListTag(TAG_TILEENTITIES);
		levelCompoundTag.put(tileEntitiesListTag);
		
		//Put data into chunk compound tag
		CompoundTag chunkCompundTag = new CompoundTag(TAG_CHUNK);
		chunkCompundTag.put(levelCompoundTag);
		chunkCompundTag.put(new IntTag(TAG_DATAVERSION, dataVersion));
		
		return chunkCompundTag;
	}
	
	public int getX() {
		return xPos;
	}
	
	public int getZ() {
		return zPos;
	}
}
