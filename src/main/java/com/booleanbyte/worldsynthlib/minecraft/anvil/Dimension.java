package com.booleanbyte.worldsynthlib.minecraft.anvil;

public enum Dimension {
	OVERWORLD, NETHER, END;
}
