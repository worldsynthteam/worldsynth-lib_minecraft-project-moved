package com.booleanbyte.worldsynthlib.minecraft.anvil;

/**
 * Based on information from
 * https://minecraft.gamepedia.com/Chunk_format#Tile_tick_format
 */

public class TileTick {
	String id; //The ID of the block; used to activate the correct block update procedure.
	int t; //The number of ticks until processing should occur. May be negative when processing is overdue.
	int p; //If multiple tile ticks are scheduled for the same tick, tile ticks with lower p will be processed first. If they also have the same p, the order is unknown.
	int x; //X position
	int y; //Y position
	int z; //Z position
	
	public TileTick(String id, int t, int p, int x, int y, int z) {
		this.id = id;
		this.t= t;
		this.p = p;
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
