package com.booleanbyte.worldsynthlib.minecraft.anvil;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

/**
 * Based on information from
 * https://minecraft.gamepedia.com/Chunk_format
 */

public class ChunkSection {
	
	public static final String TAG_SECTION = "Section";
	public static final String TAG_Y = "Y";
	public static final String TAG_BLOCKLIGHT = "BlockLight";
	public static final String TAG_SKYLIGHT = "SkyLight";
	public static final String TAG_BLOCKS = "Blocks";
	public static final String TAG_ADD = "Add";
	public static final String TAG_DATA = "Data";
	
	boolean useAdd= false;
	
	byte sectionY;
	
	int[][][] blockId = new int[16][16][16];
	byte[][][] blockData = new byte[16][16][16];
	byte[][][] blockLight = new byte[16][16][16];
	byte[][][] skyLight = new byte[16][16][16];
	
	public ChunkSection(byte sectionY, int[][][] blockId, byte[][][] blockData) {
		this.sectionY = sectionY;
		this.blockId = blockId;
		this.blockData = blockData;
		
		for(int x = 0; x < 16; x++) {
			for(int y = 0; y < 16; y++) {
				for(int z = 0; z < 16; z++) {
					if(blockId[x][y][z] > 255) {
						useAdd = true;
						return;
					}
				}
			}
		}
	}
	
	public ChunkSection(CompoundTag sectionTag) {
		sectionY = (byte) sectionTag.get(TAG_Y).getValue();
		
		useAdd = sectionTag.contains(TAG_ADD);
		
		byte[] serializedBlocks = ((ByteArrayTag) sectionTag.get(TAG_BLOCKS)).getValue();
		byte[] serializedAdd = new byte[2048];
		if(useAdd) {
			serializedAdd = ((ByteArrayTag) sectionTag.get(TAG_ADD)).getValue();
		}
		byte[] serializedData = ((ByteArrayTag) sectionTag.get(TAG_DATA)).getValue();
		byte[] serializedBlockLight = ((ByteArrayTag) sectionTag.get(TAG_BLOCKLIGHT)).getValue();
		byte[] serializedSkyLight = ((ByteArrayTag) sectionTag.get(TAG_SKYLIGHT)).getValue();
		
		//Convert
		for(int x = 0; x < 16; x++) {
			for(int y = 0; y < 16; y++) {
				for(int z = 0; z < 16; z++) {
					int index = y*256 + z*16 + x;
					byte block = serializedBlocks[index];
					
					int dataIndex = index / 2;
					int dataSubIndex = index % 2;
					
					byte bLight = serializedBlockLight[dataIndex];
					byte sLight = serializedSkyLight[dataIndex];
					byte add = serializedAdd[dataIndex];
					byte data = serializedData[dataIndex];
					
					//Block ID
					blockId[x][y][z] = block & 0xFF;
					if(useAdd) {
						if(dataSubIndex == 0) {
							int idAdd = add & 0x0F;
							blockId[x][y][z] = blockId[x][y][z] | (idAdd << 8);
						}
						else {
							int idAdd = add & 0xF0;
							blockId[x][y][z] = blockId[x][y][z] | (idAdd << 4);
						}
					}
					
					//Block data
					if(dataSubIndex == 0) {
						data = (byte) (data & 0x0F);
						blockData[x][y][z] = data;
					}
					else {
						data = (byte) ((data & 0xF0) >> 4);
						blockData[x][y][z] = data;
					}
					
					//Blocklight and skylight
					if(dataSubIndex == 0) {
						bLight = (byte) (bLight & 0x0F);
						sLight = (byte) (sLight & 0x0F);
						blockLight[x][y][z] = bLight;
						skyLight[x][y][z] = sLight;
					}
					else {
						bLight = (byte) ((bLight & 0xF0) >> 4);
						sLight = (byte) ((sLight & 0xF0) >> 4);
						blockLight[x][y][z] = bLight;
						skyLight[x][y][z] = sLight;
					}
				}
			}
		}
	}
	
	public CompoundTag toNbt() {
		byte[] blocks = new byte[4096];
		byte[] add = new byte[2048];
		byte[] data = new byte[2048];
		byte[] bLight = new byte[2048];
		byte[] sLight = new byte[2048];
		
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				
				//Blocks
				for(int y = 0; y < 16; y++) {
					int index = y*256 + z*16 + x;
					int id = blockId[x][y][z];
					blocks[index] = (byte)(id & 0xFF);
				}
			}
		}
		
		for(int x = 0; x < 16; x+=2) {
			for(int z = 0; z < 16; z++) {
				//Add
				if(useAdd) {
					for(int y = 0; y < 16; y+=2) {
						int index = y*256/2 + z*16/2 + x/2;
						int id0 = blockId[x][y][z];
						int id1 = blockId[x][y+1][z];
						
						byte add0 = (byte) ((id0 >> 8) & 0x0F);
						byte add1 = (byte) ((id1 >> 8) & 0x0F);
						
						add[index] = (byte) (add0 | (add1 << 4));
					}
				}
				
				//Data
				for(int y = 0; y < 16; y++) {
					int index = y*256/2 + z*16/2 + x/2;
					byte data0 = blockData[x][y][z];
					byte data1 = blockData[x+1][y][z];
					
					data0 = (byte) (data0 & 0x0F);
					data1 = (byte) (data1 & 0x0F);
					
					data[index] = (byte) (data0 | (data1 << 4));
				}
				
				//BlockLight
				for(int y = 0; y < 16; y+=2) {
					int index = y*256/2 + z*16/2 + x/2;
					byte light0 = blockLight[x][y][z];
					byte light1 = blockLight[x][y+1][z];
					
					light0 = (byte) (light0 & 0x0F);
					light1 = (byte) (light1 & 0x0F);
					
					bLight[index] = (byte) (light0 | (light1 << 4));
				}
				
				//SkyLight
				for(int y = 0; y < 16; y+=2) {
					int index = y*256/2 + z*16/2 + x/2;
					byte light0 = skyLight[x][y][z];
					byte light1 = skyLight[x][y+1][z];
					
					light0 = (byte) (light0 & 0x0F);
					light1 = (byte) (light1 & 0x0F);
					
					sLight[index] = (byte) (light0 | (light1 << 4));
				}
			}
		}
		
		CompoundTag sectionCompoundTag = new CompoundTag(TAG_SECTION);
		
		sectionCompoundTag.put(new ByteTag(TAG_Y, sectionY));
		sectionCompoundTag.put(new ByteArrayTag(TAG_BLOCKS, blocks));
		if(useAdd) {
			sectionCompoundTag.put(new ByteArrayTag(TAG_ADD, add));
		}
		sectionCompoundTag.put(new ByteArrayTag(TAG_DATA, data));
		sectionCompoundTag.put(new ByteArrayTag(TAG_BLOCKLIGHT, bLight));
		sectionCompoundTag.put(new ByteArrayTag(TAG_SKYLIGHT, sLight));
		
		return sectionCompoundTag;
	}
}
