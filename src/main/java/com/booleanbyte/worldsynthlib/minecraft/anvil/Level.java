package com.booleanbyte.worldsynthlib.minecraft.anvil;

import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.DoubleTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

public class Level {
	
	public static final String TAG_LEVEL                   = "Level";
    public static final String TAG_TIME                    = "Time";
    public static final String TAG_LAST_PLAYED             = "LastPlayed";
    public static final String TAG_SPAWN_X                 = "SpawnX";
    public static final String TAG_SPAWN_Y                 = "SpawnY";
    public static final String TAG_SPAWN_Z                 = "SpawnZ";
    public static final String TAG_SIZE_ON_DISK            = "SizeOnDisk";
    public static final String TAG_RANDOM_SEED             = "RandomSeed";
    public static final String TAG_VERSION                 = "version";
    public static final String TAG_LEVEL_NAME              = "LevelName";
    public static final String TAG_MAP_FEATURES            = "MapFeatures";
    public static final String TAG_GAME_TYPE               = "GameType";
    public static final String TAG_GENERATOR_NAME          = "generatorName";
    public static final String TAG_GENERATOR_VERSION       = "generatorVersion";
    public static final String TAG_TEXT1                   = "Text1";
    public static final String TAG_TEXT2                   = "Text2";
    public static final String TAG_TEXT3                   = "Text3";
    public static final String TAG_TEXT4                   = "Text4";
    public static final String TAG_ALLOW_COMMANDS          = "allowCommands";
    public static final String TAG_GENERATOR_OPTIONS       = "generatorOptions";
    public static final String TAG_HARDCORE                = "hardcore";
    public static final String TAG_DIFFICULTY              = "Difficulty";
    public static final String TAG_DIFFICULTY_LOCKED       = "DifficultyLocked";
    public static final String TAG_LIGHT_POPULATED         = "LightPopulated";
    public static final String TAG_INHABITED_TIME          = "InhabitedTime";
    public static final String TAG_BORDER_CENTER_X         = "BorderCenterX";
    public static final String TAG_BORDER_CENTER_Z         = "BorderCenterZ";
    public static final String TAG_BORDER_SIZE             = "BorderSize";
    public static final String TAG_BORDER_SAFE_ZONE        = "BorderSafeZone";
    public static final String TAG_BORDER_WARNING_BLOCKS   = "BorderWarningBlocks";
    public static final String TAG_BORDER_WARNING_TIME     = "BorderWarningTime";
    public static final String TAG_BORDER_SIZE_LERP_TARGET = "BorderSizeLerpTarget";
    public static final String TAG_BORDER_SIZE_LERP_TIME   = "BorderSizeLerpTime";
    public static final String TAG_BORDER_DAMAGE_PER_BLOCK = "BorderDamagePerBlock";
	
	private int nbtversion = 19133;
	
	private byte initialized = 0;
	
	private String levelName;
	private String generatorName;
	private int generatorVersion = 0;
	private String generatorOptions = "";
	private long randomSeed;
	private byte mapFeatures = 0;
	private long lastPlayed = System.currentTimeMillis();
	private long sizeOnDisk = 0;
	
	private byte allowCommands = 1;
	private byte hardcore = 0;
	private int gametype = 1;
	private byte difficulty = 2;
	private byte difficultyLocked = 0;
	
	private long time = 0;
	private long dayTime = 0;
	
	private int spawnX = 0;
	private int spawnY = 60;
	private int spawnZ = 0;
	
	private double borderCenterX = 0;
	private double borderCenterZ = 0;
	private double borderSize = 60000000;
	private double borderSafeZone = 5;
	private double borderWarningBlocks = 5;
	private double borderWarningTime = 15;
	private double borderSizeLerpTarget = 60000000;
	private long borderSizeLerpTime = 0;
	private double borderDamagePerBlock = 0.2;
	
	public Level(String levelName, String generatorName, long seed, double spawnX, double spawnY, double spawnZ) {
		this.levelName = levelName;
		this.generatorName = generatorName;
		this.randomSeed = seed;
	}
	
	public Level(CompoundTag levelTag) {
		nbtversion = (int) levelTag.get(TAG_VERSION).getValue();
		
		initialized = (byte) levelTag.get("initialized").getValue();
		levelName = (String) levelTag.get(TAG_LEVEL_NAME).getValue();
		generatorName = (String) levelTag.get(TAG_GENERATOR_NAME).getValue();
		generatorVersion = (int) levelTag.get(TAG_GENERATOR_VERSION).getValue();
		generatorOptions = (String) levelTag.get(TAG_GENERATOR_OPTIONS).getValue();
		randomSeed = (long) levelTag.get(TAG_RANDOM_SEED).getValue();
		mapFeatures = (byte) levelTag.get(TAG_MAP_FEATURES).getValue();
		
		lastPlayed = (long) levelTag.get(TAG_LAST_PLAYED).getValue();
		sizeOnDisk = (long) levelTag.get(TAG_SIZE_ON_DISK).getValue();
		
		allowCommands = (byte) levelTag.get(TAG_ALLOW_COMMANDS).getValue();
		hardcore = (byte) levelTag.get(TAG_HARDCORE).getValue();
		gametype = (int) levelTag.get(TAG_GAME_TYPE).getValue();
		difficulty = (byte) levelTag.get(TAG_DIFFICULTY).getValue();
		difficultyLocked = (byte) levelTag.get(TAG_DIFFICULTY_LOCKED).getValue();
		time = (long) levelTag.get(TAG_TIME).getValue();
		dayTime = (long) levelTag.get("DayTime").getValue();
		
		spawnX = (int) levelTag.get(TAG_SPAWN_X).getValue();
		spawnY = (int) levelTag.get(TAG_SPAWN_Y).getValue();
		spawnZ = (int) levelTag.get(TAG_SPAWN_Z).getValue();
		borderCenterX = (double) levelTag.get(TAG_BORDER_CENTER_X).getValue();
		borderCenterZ = (double) levelTag.get(TAG_BORDER_CENTER_Z).getValue();
		borderSize = (double) levelTag.get(TAG_BORDER_SIZE).getValue();
		borderSafeZone = (double) levelTag.get(TAG_BORDER_SAFE_ZONE).getValue();
		borderWarningBlocks = (double) levelTag.get(TAG_BORDER_WARNING_BLOCKS).getValue();
		borderWarningTime = (double) levelTag.get(TAG_BORDER_WARNING_TIME).getValue();
		borderSizeLerpTarget = (double) levelTag.get(TAG_BORDER_SIZE_LERP_TARGET).getValue();
		borderSizeLerpTime = (long) levelTag.get(TAG_BORDER_SIZE_LERP_TIME).getValue();
		borderDamagePerBlock = (long) levelTag.get(TAG_BORDER_DAMAGE_PER_BLOCK).getValue();
	}
	
	public CompoundTag toNbt() {
		CompoundTag data = new CompoundTag("Data");
		
		data.put(new IntTag(TAG_VERSION, nbtversion));
		
		//data.putByte("initialized", initialized);
		data.put(new StringTag(TAG_LEVEL_NAME, levelName));
		data.put(new StringTag(TAG_GENERATOR_NAME, generatorName));
		data.put(new IntTag(TAG_GENERATOR_VERSION, generatorVersion));
		//data.putString(TAG_GENERATOR_OPTIONS, generatorOptions);
		data.put(new LongTag(TAG_RANDOM_SEED, randomSeed));
		data.put(new ByteTag(TAG_MAP_FEATURES, mapFeatures));
		
		data.put(new LongTag(TAG_LAST_PLAYED, lastPlayed));
		//data.putLong(TAG_SIZE_ON_DISK, sizeOnDisk);
		
		data.put(new ByteTag(TAG_ALLOW_COMMANDS, allowCommands));
		data.put(new ByteTag(TAG_HARDCORE, hardcore));
		data.put(new IntTag(TAG_GAME_TYPE, gametype));
		data.put(new ByteTag(TAG_DIFFICULTY, difficulty));
		//data.putByte(TAG_DIFFICULTY_LOCKED, difficultyLocked);
		//data.putLong(TAG_TIME, time);
		//data.putLong("DayTime", dayTime);
		
		data.put(new IntTag(TAG_SPAWN_X, spawnX));
		data.put(new IntTag(TAG_SPAWN_Y, spawnY));
		data.put(new IntTag(TAG_SPAWN_Z, spawnZ));
		data.put(new DoubleTag(TAG_BORDER_CENTER_X, borderCenterX));
		data.put(new DoubleTag(TAG_BORDER_CENTER_Z, borderCenterZ));
		data.put(new DoubleTag(TAG_BORDER_SIZE, borderSize));
		data.put(new DoubleTag(TAG_BORDER_SAFE_ZONE, borderSafeZone));
		data.put(new DoubleTag(TAG_BORDER_WARNING_BLOCKS, borderWarningBlocks));
		data.put(new DoubleTag(TAG_BORDER_WARNING_TIME, borderWarningTime));
		data.put(new DoubleTag(TAG_BORDER_SIZE_LERP_TARGET, borderSizeLerpTarget));
		data.put(new LongTag(TAG_BORDER_SIZE_LERP_TIME, borderSizeLerpTime));
		data.put(new DoubleTag(TAG_BORDER_DAMAGE_PER_BLOCK, borderDamagePerBlock));
		
		return data;
	}
	
	private CompoundTag defaultGameRulesTag() {
		CompoundTag gamerules = new CompoundTag("GameRules");
		
		gamerules.put(new StringTag("announceAdvancements", "true"));
		gamerules.put(new StringTag("commandBlockOutput", "true"));
		gamerules.put(new StringTag("disableElytraMovementCheck", "false"));
		gamerules.put(new StringTag("doDaylightCycle", "true"));
		gamerules.put(new StringTag("doEntityDrops", "true"));
		gamerules.put(new StringTag("doFireTick", "true"));
		gamerules.put(new StringTag("doLimitedCrafting", "false"));
		gamerules.put(new StringTag("doMobLoot", "true"));
		gamerules.put(new StringTag("doMobSpawning", "true"));
		gamerules.put(new StringTag("doTileDrops", "true"));
		gamerules.put(new StringTag("doWeatherCycle", "true"));
		gamerules.put(new StringTag("gameLoopFunction", "-"));
		gamerules.put(new StringTag("keepInventory", "false"));
		gamerules.put(new StringTag("logAdminCommands", "true"));
		gamerules.put(new StringTag("maxCommandChainLenght", "65536"));
		gamerules.put(new StringTag("maxEntityCramming", "24"));
		gamerules.put(new StringTag("mobGriefing", "true"));
		gamerules.put(new StringTag("naturalRegeneration", "true"));
		gamerules.put(new StringTag("randomTickSpeed", "3"));
		gamerules.put(new StringTag("reduceDebugInfo", "true"));
		gamerules.put(new StringTag("sendCommandFeedback", "true"));
		gamerules.put(new StringTag("showDethMessages", "true"));
		gamerules.put(new StringTag("spawnRadius", "10"));
		gamerules.put(new StringTag("spectatorsGenerateChunks", "true"));
		
		return gamerules;
	}
}
