package com.booleanbyte.worldsynthlib.minecraft.anvil;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class AnvilImporter {
	private static final String NETHER_FOLDER = "DIM-1";
    private static final String ENDER_FOLDER = "DIM1";
    
    private File overworldRegionFolder;
    private File netherRegionFolder;
    private File endRegionFolder;
	
	private File levelDirectory;
	
	private String levelId;
	
	public AnvilImporter(File levelDirectory) {
		this.levelDirectory = levelDirectory;
		
		if(!isSave()) {
			return;
		}

		overworldRegionFolder = new File(levelDirectory, "region");
		netherRegionFolder = new File(levelDirectory, NETHER_FOLDER + "/region");
		endRegionFolder = new File(levelDirectory, ENDER_FOLDER + "/region");
	}
	
	public boolean isSave() {
		File levelFile = new File(levelDirectory, "level.dat");
		if(!levelFile.exists()) {
			return false;
		}
		
		return true;
	}
	
	public Chunk readChunkFromFile(int x, int z, Dimension dim) {
		int regionX = (int) Math.floor((double) x / 32.0);
		int regionZ = (int) Math.floor((double) z / 32.0);
		
		int regionChunkX = x - regionX * 32;
		int regionChunkZ = z - regionZ * 32;
		
		RegionFile region = null;
		if(dim == Dimension.OVERWORLD) {
			region = new RegionFile(overworldRegionFolder, regionX, regionZ);
		}
		else if(dim == Dimension.NETHER) {
			region = new RegionFile(netherRegionFolder, regionX, regionZ);
		}
		else if(dim == Dimension.END) {
			region = new RegionFile(endRegionFolder, regionX, regionZ);
		}
		
		InputStream chunkDataInputStream = region.getChunkDataInputStream(regionChunkX, regionChunkZ);
		if(chunkDataInputStream == null) {
			return null;
		}
		try {
			CompoundTag chunkCompoundTag = (CompoundTag) NBTIO.readTag(chunkDataInputStream);
			chunkDataInputStream.close();
			region.close();
			Chunk chunk = new Chunk(chunkCompoundTag);
			return chunk;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
