package com.booleanbyte.worldsynthlib.minecraft.module.anvil;

import java.awt.GridBagConstraints;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeBiomemap;
import com.booleanbyte.worldsynth.datatype.DatatypeBlockspace;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.extent.WorldExtent;
import com.booleanbyte.worldsynth.extent.WorldExtentParameter;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import com.booleanbyte.worldsynth.standalone.ui.parameters.FileParameterField;
import com.booleanbyte.worldsynth.standalone.ui.parameters.IntegerParameterField;
import com.booleanbyte.worldsynth.standalone.ui.parameters.StringParameterField;
import com.booleanbyte.worldsynth.standalone.ui.stage.UiWindowUtil;
import com.booleanbyte.worldsynth.synth.io.Element;
import com.booleanbyte.worldsynthlib.minecraft.MinecraftModuleCategories;
import com.booleanbyte.worldsynthlib.minecraft.anvil.Dimension;

import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.WindowEvent;

public class ModuleMinecraftWorldExport extends AbstractModule {
	
	private WorldExtentParameter exportExtent;
	private int chunkGroupingX = 4;
	private int chunkGroupingZ = 4;
	private boolean chunksDecorated = true;
	private WorldGenerator worldGenerator = WorldGenerator.DEFAULT;
	private Dimension worldDimension = Dimension.OVERWORLD;
	private File saveDirectory = new File(findMinecraftDir(), "saves");
	private String saveName = "New WorldSynth export";
	
	@Override
	protected void postInit() {
		exportExtent = getNewExtentParameter();
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("input");
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Minecraft world export";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return MinecraftModuleCategories.MINECRAFT;
	}

	@Override
	public ModuleInput[] registerInputs() {
			ModuleInput[] i = {
					new ModuleInput(new DatatypeBlockspace(), "Blockspace input"),
					new ModuleInput(new DatatypeBiomemap(), "Biomemap input")
					};
			return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Blockspace trough", false)
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ExtentParameterDropdownSelector parameterExtent = exportExtent.getDropdownSelector("Export extent");
		
		IntegerParameterField parameterGroupingX = new IntegerParameterField("Chunk grouping x", chunkGroupingX);
		IntegerParameterField parameterGroupingZ = new IntegerParameterField("Chunk grouping z", chunkGroupingZ);
		
		BooleanParameterCheckbox parameterChunksDecorated = new BooleanParameterCheckbox("Chunks decorated", chunksDecorated);
		EnumParameterDropdownSelector<WorldGenerator> parameterWorldGenerator = new EnumParameterDropdownSelector<WorldGenerator>("Generator", WorldGenerator.class, worldGenerator);
		EnumParameterDropdownSelector<Dimension> parameterDimension = new EnumParameterDropdownSelector<Dimension>("Dimension", Dimension.class, worldDimension);
		
		FileParameterField parameterSaveDirectory = new FileParameterField("Minecraft save directory", saveDirectory, true, true, null);
		StringParameterField parameterLevelName = new StringParameterField("Level name", saveName);
		
		
		Button exportButton = new Button("Export world");
		exportButton.setOnAction(e -> {
				exportWorldFile(parameterSaveDirectory.getValue(), parameterLevelName.getValue(), parameterExtent.getValue(), parameterGroupingX.getValue(),
						parameterGroupingZ.getValue(), Dimension.OVERWORLD, parameterWorldGenerator.getValue(), parameterChunksDecorated.getValue(), pane);
		});
		
		parameterExtent.addToGrid(pane, 0);
		parameterGroupingX.addToGrid(pane, 1);
		parameterGroupingZ.addToGrid(pane, 2);
		parameterDimension.addToGrid(pane, 3);
		parameterChunksDecorated.addToGrid(pane, 4);
		parameterWorldGenerator.addToGrid(pane, 5);
		parameterSaveDirectory.addToGrid(pane, 6);
		parameterLevelName.addToGrid(pane, 7);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 1;
		gbc.gridy = 8;
		pane.add(exportButton, 1, 9);
		
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			exportExtent.setExtent(parameterExtent.getValue());
			
			chunkGroupingX = parameterGroupingX.getValue();
			chunkGroupingZ = parameterGroupingZ.getValue();
			
			chunksDecorated = parameterChunksDecorated.getValue();
			worldGenerator = parameterWorldGenerator.getValue();
			worldDimension = parameterDimension.getValue();
			
			saveDirectory = parameterSaveDirectory.getValue();
			saveName = parameterLevelName.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("extent", exportExtent.getExtentAsString()));
		
		paramenterElements.add(new Element("groupingx", String.valueOf(chunkGroupingX)));
		paramenterElements.add(new Element("groupingz", String.valueOf(chunkGroupingZ)));
		
		paramenterElements.add(new Element("decorated", String.valueOf(chunksDecorated)));
		paramenterElements.add(new Element("generator", worldGenerator.name()));
		paramenterElements.add(new Element("dimension", worldDimension.name()));
		
		paramenterElements.add(new Element("savedirectory", saveDirectory.getPath()));
		paramenterElements.add(new Element("savename", saveName));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("extent")) {
				exportExtent.setExtentAsString(e.content);
			}
			else if(e.tag.equals("groupingx")) {
				chunkGroupingX = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("groupingz")) {
				chunkGroupingZ = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("decorated")) {
				chunksDecorated = Boolean.parseBoolean(e.content);
			}
			else if(e.tag.equals("generator")) {
				for(WorldGenerator type: WorldGenerator.values()) {
					if(e.content.equals(type.name())) {
						worldGenerator = type;
						break;
					}
				}
			}
			else if(e.tag.equals("dimension")) {
				for(Dimension type: Dimension.values()) {
					if(e.content.equals(type.name())) {
						worldDimension = type;
						break;
					}
				}
			}
			else if(e.tag.equals("savedirectory")) {
				saveDirectory = new File(e.content);
			}
			else if(e.tag.equals("savename")) {
				saveName = e.content;
			}
		}
	}
	
	public static File findMinecraftDir() {
        File candidate;
        String appData = System.getenv("APPDATA");
        if (appData != null) {
            candidate = new File(appData, ".minecraft");
            if (candidate.isDirectory()) {
                return candidate;
            }
        }
        candidate = new File(System.getProperty("user.home"), "Library/Application Support/minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        candidate = new File(System.getProperty("user.home"), ".minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        return null;
    }
	
	private void exportWorldFile(File minecraftSaveDirectory, String levelName, WorldExtent extent, int xGrouping, int zGrouping, Dimension dimension, WorldGenerator generator, boolean decorated, Pane parentPane) {
		File worldSaveDirectory = new File(minecraftSaveDirectory, levelName);
		
		if(worldSaveDirectory.exists()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Export world exists");
			alert.setHeaderText("WARNING: This world save already exists, do you want to overwrite it?");
			alert.setContentText("Are you sure you want to overwrite this world save.\nThe old world will be lost forever! (A long time)");
			
			ButtonType buttonTypeAbort = new ButtonType("Abort");
			ButtonType buttonTypeOverwrite = new ButtonType("Overwrite");
			
			alert.getButtonTypes().setAll(buttonTypeAbort, buttonTypeOverwrite);
			Optional<ButtonType> result = alert.showAndWait();
			
			if(result.get() != buttonTypeOverwrite) return;
		}
		
		ChunkRegion buildRegion = new ChunkRegion((int) extent.getX(), (int) extent.getZ(), (int) extent.getWidth(), (int) extent.getLength());
		WorldExportTask task = new WorldExportTask(ModuleMinecraftWorldExport.this, getInput(0), getInput(1), minecraftSaveDirectory, levelName, dimension, generator, decorated, buildRegion, xGrouping, zGrouping, parentPane);
		
		EventHandler<WindowEvent> exportprogressWindowCloseEvent = EventHandler -> {
			task.cancel();
		};
		
		ToggleButton pinButton = UiWindowUtil.constructPinButton();
		UiWindowUtil.openPinnableUtilityWindow(new ExportProgressPane(buildRegion, task, pinButton), "Minecraft worldExport", true, exportprogressWindowCloseEvent, pinButton.selectedProperty());
		new Thread(task, "Minecraft world exporter task thread").start();
	}
}
