package com.booleanbyte.worldsynthlib.minecraft.module.anvil;

import java.io.File;
import java.util.ArrayList;

import com.booleanbyte.worldsynth.datatype.DatatypeBiomemap;
import com.booleanbyte.worldsynth.datatype.DatatypeBlockspace;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynthlib.minecraft.anvil.AnvilExporter;
import com.booleanbyte.worldsynthlib.minecraft.anvil.Chunk;
import com.booleanbyte.worldsynthlib.minecraft.anvil.Dimension;

import javafx.concurrent.Task;
import javafx.scene.layout.Pane;

public class WorldExportTask extends Task<Boolean> {
	
	private final ModuleMinecraftWorldExport exportModule;
	private final ModuleInput blockspaceInput;
	private final ModuleInput biomemapInput;
	
	private final File minecraftSaveDirectory;
	private final String levelName;
	private final String generatorName;
	private final Dimension dimension;
	private final boolean setDecorated;
	
	private final int groupingX;
	private final int groupingZ;
	
	private final int startChunkX;
	private final int startChunkZ;
	private final int endChunkX;
	private final int endChunkZ;
	private final int chunkCountX;
	private final int chunkCountZ;
	
	private volatile ArrayList<ChunkCoordinate> writtenChunks = new ArrayList<ChunkCoordinate>();
	
	public WorldExportTask(ModuleMinecraftWorldExport exportModule, ModuleInput blockspaceInput, ModuleInput biomemapInput, File minecraftSaveDirectory, String levelName, Dimension dimension, WorldGenerator generator, boolean setDecorated, ChunkRegion buildRegion, int groupingX, int groupingZ, Pane relativePositionComponent) {
		this.exportModule = exportModule;
		this.blockspaceInput = blockspaceInput;
		this.biomemapInput = biomemapInput;
		
		this.minecraftSaveDirectory = minecraftSaveDirectory;
		this.levelName = levelName;
		this.generatorName = generator.getName();
		
		this.dimension = dimension;
		this.setDecorated = setDecorated;
		
		if(groupingX < 1) {
			groupingX = 1;
		}
		if(groupingZ < 1) {
			groupingZ = 1;
		}
		this.groupingX = groupingX;
		this.groupingZ = groupingZ;
		
		startChunkX = buildRegion.getStartX();
		startChunkZ = buildRegion.getStartZ();
		endChunkX = buildRegion.getEndX();
		endChunkZ = buildRegion.getEndZ();
		chunkCountX = buildRegion.getWidth();
		chunkCountZ = buildRegion.getLenght();
		
		setOnFailed(e -> {
			System.err.println("Minecraft world export failed");
			getException().printStackTrace();
		});
	}
	
	@Override
	protected Boolean call() throws Exception {
		AnvilExporter exporter = new AnvilExporter(minecraftSaveDirectory, levelName, generatorName);
		
		int currentBuiltChunkCount = 0;
		
		int xGroupsCount = (int) Math.ceil((double) chunkCountX / (double) groupingX);
		int zGroupsCount = (int) Math.ceil((double) chunkCountZ / (double) groupingZ);
		
		for(int cgx = 0; cgx < xGroupsCount; cgx++) {
			for(int cgz = 0; cgz < zGroupsCount; cgz++) {
				if(isCancelled()) {
					return null;
				}
				int groupStartChunkX = cgx*groupingX + startChunkX;
				int groupStartChunkZ = cgz*groupingZ + startChunkZ;
				int groupEndChunkX = cgx*groupingX + startChunkX + groupingX;
				int groupEndChunkZ = cgz*groupingZ + startChunkZ + groupingZ;
				
				if(groupEndChunkX > endChunkX+1) {
					groupEndChunkX = endChunkX+1;
				}
				if(groupEndChunkZ > endChunkZ+1) {
					groupEndChunkZ = endChunkZ+1;
				}
				
				double buildStartX = groupStartChunkX * 16;
				double buildStartZ = groupStartChunkZ * 16;
				double buildWidth = (groupEndChunkX - groupStartChunkX) * 16;
				double buildLenght = (groupEndChunkZ - groupStartChunkZ) * 16;
				
				ModuleInputRequest request = new ModuleInputRequest(blockspaceInput, new DatatypeBlockspace(buildStartX, 0.0, buildStartZ, buildWidth, 256.0, buildLenght));
				DatatypeBlockspace blockspace = (DatatypeBlockspace) exportModule.buildInputData(request);
				request = new ModuleInputRequest(biomemapInput, new DatatypeBiomemap(buildStartX, buildStartZ, buildWidth, buildLenght));
				DatatypeBiomemap biomemap = (DatatypeBiomemap) exportModule.buildInputData(request);
				
				//Write built data to file
				for(int u = 0; u < groupingX; u++) {
					for(int v = 0; v < groupingZ; v++) {
						int writeChunkX = groupStartChunkX+u;
						int writeChunkZ = groupStartChunkZ+v;
						
						if(writeChunkX > endChunkX || writeChunkZ > endChunkZ) {
							continue;
						}
						
						int[][][] blockspaceMaterialId;
						int[][] biomeMap = new int[16][16];
						
						if(groupingX > 1 || groupingZ > 1) {
							blockspaceMaterialId = extractSubBlockspace(blockspace.blockspaceMaterialId, u*16, 0, v*16, 16, 256, 16);
							if(biomemap != null) {
								biomeMap = extractSubBiomemap(biomemap.biomeMap, u*16, v*16, 16, 16);
							}
						}
						else {
							blockspaceMaterialId = blockspace.blockspaceMaterialId;
							if(biomemap != null) {
								biomeMap = biomemap.biomeMap;
							}
						}
						
						
						Chunk chunk = new Chunk(writeChunkX, writeChunkZ, biomeMap, blockspaceMaterialId, setDecorated);
						exporter.writeChunkToFile(chunk, dimension);
						
						currentBuiltChunkCount++;
						writtenChunks.add(new ChunkCoordinate(writeChunkX, writeChunkZ));
						//FIXME Fix possible/problale race problem
						System.out.println("Wrote chunk: " + writeChunkX + "," + writeChunkZ);
						updateMessage("Wrote chunk: " + writeChunkX + "," + writeChunkZ);
					}
				}
			}
		}
		return true;
	}
	
	private int[][][] extractSubBlockspace(int[][][] id, int x, int y, int z, int width, int height, int lenght) {
		int[][][] extractedId = new int[width][height][lenght];
		
		for(int u = 0; u < width; u++) {
			for(int v = 0; v < height; v++) {
				for(int w = 0; w < lenght; w++) {
					extractedId[u][v][w] = id[x+u][y+v][z+w];
				}
			}
		}
		
		return extractedId;
	}
	
	private int[][] extractSubBiomemap(int[][] biomemap, int x, int z, int width, int lenght) {
		int[][] extractedBiomemap = new int[width][lenght];
		
		for(int u = 0; u < width; u++) {
			for(int w = 0; w < lenght; w++) {
				extractedBiomemap[u][w] = biomemap[x+u][z+w];
			}
		}
		
		return extractedBiomemap;
	}
	
	public ArrayList<ChunkCoordinate> getWrittenChunksList() {
		return (ArrayList<ChunkCoordinate>) writtenChunks.clone();
	}
}
