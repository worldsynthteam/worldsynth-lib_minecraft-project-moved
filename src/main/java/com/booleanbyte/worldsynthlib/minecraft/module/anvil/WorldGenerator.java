package com.booleanbyte.worldsynthlib.minecraft.module.anvil;

public enum WorldGenerator {
	DEFAULT ("default"),
	LARGEBIOMES ("largeBiomes"),
	FLAT ("flat"),
	AMPLIFIED ("amplified"),
	WORLDSYNTH ("worldsynth");
	
	private final String name;
	
	private WorldGenerator(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
