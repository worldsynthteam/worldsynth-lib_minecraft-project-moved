package com.booleanbyte.worldsynthlib.minecraft.module.anvil;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class ExportProgressPane extends GridPane {
	
	ProgressBar exportProgressBar;
	Label exportProgressLabel;
	ChunkView chunkView;
	
	public ExportProgressPane(ChunkRegion buildRegion, WorldExportTask exportTask, ToggleButton pinButton) {
		chunkView = new ChunkView(500, 500, buildRegion, exportTask);
		GridPane.setHgrow(chunkView, Priority.ALWAYS);
		GridPane.setVgrow(chunkView, Priority.ALWAYS);
		add(chunkView, 0, 1);
		
		exportProgressBar = new ProgressBar(0.0D);
		exportProgressBar.setPrefWidth(300);
		exportProgressLabel = new Label("  0%  |  0 / " + buildRegion.chunkCount() + " chunks written");
		
		HBox progressBox = new HBox(exportProgressBar, exportProgressLabel);
		progressBox.setAlignment(Pos.CENTER);
		add(progressBox, 0, 0);
		
		GridPane.setHalignment(pinButton, HPos.LEFT);
		add(pinButton, 0, 2);
		
		Button abortButton = new Button("Abort world export");
		abortButton.setPrefWidth(200);
		abortButton.setOnAction(e -> {
			exportTask.cancel();
		});
		GridPane.setHalignment(abortButton, HPos.CENTER);
		add(abortButton, 0, 2);
		
		exportTask.messageProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				int writtenChnkCount = exportTask.getWrittenChunksList().size();
				double exportProgress = (double) writtenChnkCount / (double) buildRegion.chunkCount();
				DecimalFormat df = new DecimalFormat("#");
				df.setRoundingMode(RoundingMode.HALF_UP);
				String exsportProgressString = df.format(exportProgress * 100.0D);
				
				exportProgressBar.setProgress(exportProgress);
				exportProgressLabel.setText("  " + exsportProgressString + "%  |  " + writtenChnkCount + " / " + buildRegion.chunkCount() + " chunks written");
			}
		});
	}
}