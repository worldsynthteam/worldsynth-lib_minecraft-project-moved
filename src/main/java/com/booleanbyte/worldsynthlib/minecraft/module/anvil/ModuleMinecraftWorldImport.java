package com.booleanbyte.worldsynthlib.minecraft.module.anvil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeBiomemap;
import com.booleanbyte.worldsynth.datatype.DatatypeBlockspace;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.FileParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;
import com.booleanbyte.worldsynthlib.minecraft.MinecraftModuleCategories;
import com.booleanbyte.worldsynthlib.minecraft.anvil.AnvilImporter;
import com.booleanbyte.worldsynthlib.minecraft.anvil.Chunk;
import com.booleanbyte.worldsynthlib.minecraft.anvil.Dimension;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleMinecraftWorldImport extends AbstractModule {
	
	File minecraftLevelSave = new File(findMinecraftDir(), "saves");
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		AnvilImporter importer = new AnvilImporter(minecraftLevelSave);
		if(!importer.isSave()) {
			return null;
		}
		
		if(request.output == getOutput(0)) {
			DatatypeBlockspace blockspaceRequest = (DatatypeBlockspace) request.data;
			int spw = blockspaceRequest.spacePointsWidth;
			int sph = blockspaceRequest.spacePointsHeight;
			int spl = blockspaceRequest.spacePointsLenght;
			
			int startChunkX = (int) Math.floor(blockspaceRequest.x / 16.0);
			int startChunkZ = (int) Math.floor(blockspaceRequest.z / 16.0);
			int endChunkX = (int) Math.ceil((blockspaceRequest.x + blockspaceRequest.width) / 16.0);
			int endChunkZ = (int) Math.ceil((blockspaceRequest.z + blockspaceRequest.lenght) / 16.0);
			
			//Extract necessary data from the read chunks
			int[][][] requestMaterialId = new int[spw][sph][spl];
			
			blockspaceRequest.blockspaceMaterialId = requestMaterialId;
			
			//Read in needed chunks
			for(int cx = startChunkX; cx < endChunkX; cx++) {
				for(int cz = startChunkZ; cz < endChunkZ; cz++) {
					Chunk chunk = importer.readChunkFromFile(cx, cz, Dimension.OVERWORLD);
					if(chunk == null) {
						continue;
					}
					putChunkBlockspace(blockspaceRequest, chunk);
				}
			}
			
			return blockspaceRequest;
		}
		else if(request.output == getOutput(1)) {
			DatatypeHeightmap heightmapRequest = (DatatypeHeightmap) request.data;
			int mpw = heightmapRequest.mapPointsWidth;
			int mph = heightmapRequest.mapPointsLength;
			
			int startChunkX = (int) Math.floor(heightmapRequest.x / 16.0);
			int startChunkZ = (int) Math.floor(heightmapRequest.z / 16.0);
			int endChunkX = (int) Math.ceil((heightmapRequest.x + heightmapRequest.width) / 16.0);
			int endChunkZ = (int) Math.ceil((heightmapRequest.z + heightmapRequest.length) / 16.0);
			
			//Extract necessary data from the read chunks
			float[][] requestHeightmap = new float[mpw][mph];
			
			heightmapRequest.heightMap = requestHeightmap;
			
			//Read in needed chunks
			for(int cx = startChunkX; cx < endChunkX; cx++) {
				for(int cz = startChunkZ; cz < endChunkZ; cz++) {
					Chunk chunk = importer.readChunkFromFile(cx, cz, Dimension.OVERWORLD);
					if(chunk == null) {
						continue;
					}
					putChunkHeightmap(heightmapRequest, chunk);
				}
			}
			
			return heightmapRequest;
		}
		else if(request.output == getOutput(2)) {
			DatatypeBiomemap biomemapRequest = (DatatypeBiomemap) request.data;
			int mpw = biomemapRequest.mapPointsWidth;
			int mph = biomemapRequest.mapPointsLength;
			
			int startChunkX = (int) Math.floor(biomemapRequest.x / 16.0);
			int startChunkZ = (int) Math.floor(biomemapRequest.z / 16.0);
			int endChunkX = (int) Math.ceil((biomemapRequest.x + biomemapRequest.width) / 16.0);
			int endChunkZ = (int) Math.ceil((biomemapRequest.z + biomemapRequest.length) / 16.0);
			
			//Extract necessary data from the read chunks
			int[][] requestBiomemap = new int[mpw][mph];
			
			biomemapRequest.biomeMap = requestBiomemap;
			
			//Read in needed chunks
			for(int cx = startChunkX; cx < endChunkX; cx++) {
				for(int cz = startChunkZ; cz < endChunkZ; cz++) {
					Chunk chunk = importer.readChunkFromFile(cx, cz, Dimension.OVERWORLD);
					if(chunk == null) {
						continue;
					}
					putChunkBiomemap(biomemapRequest, chunk);
				}
			}
			
			return biomemapRequest;
		}
		
		return null;
	}
	
	private void putChunkBlockspace(DatatypeBlockspace blockspace, Chunk chunk) {
		int chunkX = chunk.getX();
		int chunkZ = chunk.getZ();
		
		int[][][] chunkMaterialId = chunk.getBlockspaceMaterial();
		
		for(int x = 0; x < 16; x++) {
			for(int y = 0; y < 256; y++) {
				for(int z = 0; z < 16; z++) {
					int globalX = chunkX*16 + x;
					int globalY = y;
					int globalZ = chunkZ*16 + z;
					
					int lx = (int) Math.floor(((double) globalX - blockspace.x) / blockspace.resolution);
					if(lx < 0 || lx >= blockspace.spacePointsWidth) {
						continue;
					}
					int ly = (int) Math.floor(((double) globalY - blockspace.y) / blockspace.resolution);
					if(ly < 0 || ly >= blockspace.spacePointsHeight) {
						continue;
					}
					int lz = (int) Math.floor(((double) globalZ - blockspace.z) / blockspace.resolution);
					if(lz < 0 || lz >= blockspace.spacePointsLenght) {
						continue;
					}
					
					blockspace.blockspaceMaterialId[lx][ly][lz] = chunkMaterialId[x][y][z];
				}
			}
		}
	}
	
	private void putChunkHeightmap(DatatypeHeightmap heightmap, Chunk chunk) {
		int chunkX = chunk.getX();
		int chunkZ = chunk.getZ();
		
		int[][] chunkHeightmap = chunk.getHeightmap();
		
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				int globalX = chunkX*16 + x;
				int globalZ = chunkZ*16 + z;
				
				int lx = (int) Math.floor(((double) globalX - heightmap.x) / heightmap.resolution);
				if(lx < 0 || lx >= heightmap.mapPointsWidth) {
					continue;
				}
				int lz = (int) Math.floor(((double) globalZ - heightmap.z) / heightmap.resolution);
				if(lz < 0 || lz >= heightmap.mapPointsLength) {
					continue;
				}
				
				heightmap.heightMap[lx][lz] = (float) (chunkHeightmap[x][z] / 256.0);
			}
		}
	}
	
	private void putChunkBiomemap(DatatypeBiomemap biomemap, Chunk chunk) {
		int chunkX = chunk.getX();
		int chunkZ = chunk.getZ();
		
		int[][] chunkBiomemap = chunk.getBiomeMap();
		
		for(int x = 0; x < 16; x++) {
			for(int z = 0; z < 16; z++) {
				int globalX = chunkX*16 + x;
				int globalZ = chunkZ*16 + z;
				
				int lx = (int) Math.floor(((double) globalX - biomemap.x) / biomemap.resolution);
				if(lx < 0 || lx >= biomemap.mapPointsWidth) {
					continue;
				}
				int lz = (int) Math.floor(((double) globalZ - biomemap.z) / biomemap.resolution);
				if(lz < 0 || lz >= biomemap.mapPointsLength) {
					continue;
				}
				
				biomemap.biomeMap[lx][lz] = chunkBiomemap[x][z] & 0xff;
			}
		}
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Minecraft world import";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return MinecraftModuleCategories.MINECRAFT;
	}

	@Override
	public ModuleInput[] registerInputs() {
			return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "World blockspace"),
				new ModuleOutput(new DatatypeHeightmap(), "Word heightmap"),
				new ModuleOutput(new DatatypeBiomemap(), "World biomemap"),
				//new ModuleOutput(new DatatypeHeightmap(), "World exists")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		FileParameterField parameterSaveDirectory = new FileParameterField("Minecraft save file directory", minecraftLevelSave, false, true, null);
		
		parameterSaveDirectory.addToGrid(pane, 0);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			minecraftLevelSave = parameterSaveDirectory.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("savepath", minecraftLevelSave.getAbsolutePath()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("savepath")) {
				minecraftLevelSave = new File(e.content);
			}
		}
	}
	
	public static File findMinecraftDir() {
        File candidate;
        String appData = System.getenv("APPDATA");
        if (appData != null) {
            candidate = new File(appData, ".minecraft");
            if (candidate.isDirectory()) {
                return candidate;
            }
        }
        candidate = new File(System.getProperty("user.home"), "Library/Application Support/minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        candidate = new File(System.getProperty("user.home"), ".minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        return null;
    }
}
