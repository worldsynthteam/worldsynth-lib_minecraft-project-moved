package com.booleanbyte.worldsynthlib.minecraft.module.anvil;

public class ChunkCoordinate {
	long x;
	long z;
	
	public ChunkCoordinate(long x, long z) {
		this.x = x;
		this.z = z;
	}
}
